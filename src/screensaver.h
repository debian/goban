/*
 * src/screensaver.h, part of Complete Goban (game program)
 * Copyright (C) 1998 Scott Draves.
 * See "configure.h.in" for more copyright information.
 */


#ifndef  _SCREENSAVER_H_
#define  _SCREENSAVER_H_  1

#ifndef  _SGF_H_
#include "sgf.h"
#endif
#ifndef  _GOGAME_H_
#include "goGame.h"
#endif
#ifndef  _GOPIC_H_
#include "goPic.h"
#endif
#ifndef  _GOBAN_H_
#include "goban.h"
#endif
#ifndef  _LOCAL_H_
#include "local.h"
#endif



/**********************************************************************
 * Data types
 **********************************************************************/

typedef struct Screensaver_struct  {
  Cgoban  *cg;

  int move;

  GoGame  *game;
  Goban  *goban;
  Sgf  *moves;

  double stonetime;
  double gametime;
  double acceleration;
  double mintime;
  char *game_dir;

  ButTimer  *timer;

  MAGIC_STRUCT
} Screensaver;


/**********************************************************************
 * Functions
 **********************************************************************/
extern Screensaver  *screensaver_create(Cgoban *cg);
extern void  screensaver_destroy(Screensaver *l);


#endif  /* _SCREENSAVER_H_ */
