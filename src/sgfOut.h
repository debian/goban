/*
 * src/sgfOut.h, part of Complete Goban (game program)
 * Copyright (C) 1995 William Shubert.
 * See "configure.h.in" for more copyright information.
 */


#ifndef  _SGFOUT_H_
#define  _MOVECHAIN_H_  1

#ifndef  _SGF_H_
#include "sgf.h"
#endif


/**********************************************************************
 * Functions
 **********************************************************************/
extern bool  sgf_writeFile(Sgf *mc, const char *fname, int *err);


#endif  /* _SGFOUT_H_ */
