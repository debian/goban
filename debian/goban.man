.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH GOBAN 1 "November  4, 2010"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
goban \- Goban screensaver
.SH SYNOPSIS
.B goban
.RI [ options ]
.SH DESCRIPTION
This manual page documents briefly the
.B goban
command.
.PP
.\" TeX users may be more comfortable with the \fB<whatever>\fP and
.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
.\" respectively.
\fBgoban\fP is a screensaver program that replays historical games of go
(aka wei-chi and baduk) on the screen.
.SH OPTIONS
.TP
.B \-version, \-\-version 
Print version information
.TP
.B \-[no]stealth 
Stealth mode
.TP
.B \-display
Display to use
.TP
.B \-[no]color 
Use color if possible
.TP
.B \-[no]hiContrast
Use high-contrast graphics instead of rendered
.TP
.B \-adfile 
Set resource file name
.TP
.B \-name 
Name used to look up X resources
.TP
.B \-fontHeight 
Size (in pixels) of font (default = 12)
.TP
.B \-window-id 
X window id to draw in
.TP
.B \-root 
Draw on the root window
.TP
.B \-stonetime 
Milliseconds between stones in screensaver (default = 2000)
.TP
.B \-gametime 
Milliseconds between games in screensaver (default = 10000)
.TP
.B \-acceleration 
Rate of acceleration of stonetime 0=slow 100=fast (default = 50)
.TP
.B \-game-dir 
Directory to find games to play
.TP
.B \-minstonetime 
Minimum stonetime (limit on acceleration) in screensaver (default = 100)
.TP
.B \-arena.games 
Number of games to play in arena test
.TP
.B \-arena.prog1 
Program 1 for arena testing
.TP
.B \-arena.prog2 
Program 2 for arena testing
.TP
.B \-arena.size 
Size to use in arena testing
.TP
.B \-arena.komi 
Komi for arena testing
.TP
.B \-arena.handicap 
Handicap for arena testing
.TP
.B \-edit
Edit SGF file
.TP
.B \-nngs
Connect to NNGS
.TP
.B \-igs 
Connect to IGS
.TP
.B \-screensaver 
Run as a screensaver
.TP
.B \-help, \-\-help 
Show help message
.SH SEE ALSO
.BR xscreensaver (1)
.PP
See the README for info about arena mode.
.SH AUTHOR
goban was written by Scott Draves <spot@draves.org>.
.PP
This manual page was written by Al Nikolov <clown@debian.org>,
for the Debian project (and may be used by others).
.PP
The Debian package is dedicated to the Saint-Petersburg Go Federation
and personally to Maxim Podolyak, it's President.
